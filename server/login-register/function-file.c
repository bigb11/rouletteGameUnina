//
//  function-file.c
//  rouletteGame
//
//  Created by Domenico Maione on 03/05/21.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "function-file.h"
#include "../../string/utilsstring.h"
#include "../../list/list.h"

#define BUFFER_MAX 128
#define BUFFER_DATI 30
#define BUFFER_COINS 4


int isEmpty(FILE *fp)
{
    int size;
    if (fp != NULL) {
        fseek (fp, 0, SEEK_END);
        size = (int) ftell(fp);
        fseek(fp, 0, SEEK_SET); //mette l'indicatore all'inizio del file
        if (0 == size) {
            return 1;
        }
    }
    return 0;
}



int check(char *str, char *str2, int scelta, char *dati_login)
{
    
    FILE *fp;
    users tmp_user[BUFFER_MAX];
    
    int i=0;
    
    char *filename = "./database/players.txt";
    
    fp = fopen(filename, "r");
    
    switch(scelta)
    {
        case 1:
            
            if(isEmpty(fp))
                return 0;
            
            else
            {
                while (fscanf(fp, "%s %s %s %s %s %s\n",tmp_user[i].user,tmp_user[i].nome,tmp_user[i].cognome,tmp_user[i].email,tmp_user[i].passw,tmp_user[i].coins) != EOF)
                {
                    
                    if(strcmp(str, tmp_user[i].user) == 0 || strcmp(str2, tmp_user[i].email) == 0)
                    {
                        fclose(fp);
                        return  1;
                    }
                    
                    i++;
                }
                fclose(fp);
                return 0;
                
            } //end else isEmpty
            
            break;
            
            
        case 2:
            
            if(isEmpty(fp))
                return 1;
            
            else
            {
                while (fscanf(fp, "%s %s %s %s %s %s\n",tmp_user[i].user,tmp_user[i].nome,tmp_user[i].cognome,tmp_user[i].email,tmp_user[i].passw,tmp_user[i].coins) != EOF)
                {
                    
                    if(strcmp(str, tmp_user[i].user) == 0 && strcmp(str2, tmp_user[i].passw) == 0)
                    {
                        
                        int lenFormat = (strlen(" ")*5);
                        int lenDati= (int) (strlen(tmp_user[i].user) + strlen(tmp_user[i].nome) + strlen(tmp_user[i].cognome) + strlen(tmp_user[i].email) + strlen(tmp_user[i].passw) +strlen(tmp_user[i].coins));
                        int lenTot = lenFormat + lenDati + 1;
                        
                        snprintf(dati_login,lenTot,"%s %s %s %s %s %s",tmp_user[i].user,tmp_user[i].nome,tmp_user[i].cognome,tmp_user[i].email,tmp_user[i].passw,tmp_user[i].coins);
                        
                        
                        
                        fclose(fp);
                        return  0;
                    }
                }
                fclose(fp);
                return 1;
                
            } //end else isEmpty
            
            break;
            
            
    }
    
    return -1;
}



char *stringWithFormat(const char *str, const char *startFormat, const char *endFormat)
{
    
        int len = (int) (strlen(str) + strlen(startFormat) + strlen(endFormat) + 1);
        char *res = calloc(len,sizeof(char));
        
        snprintf(res, len, "%s%s%s",startFormat,str,endFormat);
        return res;
    
    
}


//Inserisce all'interno di un file txt gli utenti che si sono loggati
void logPlayersTxt(List listPlayer)
{
    FILE *fp;
    
    fp = fopen("./database/loggers.txt", "a");
    fprintf(fp,"U:[%s] C:[%d] DATE:[%s]\n",listPlayer->elem->username,listPlayer->elem->coins,listPlayer->date);
    fclose(fp);
    
}


void storyBoard(BidList topBid)
{
    FILE *fp;
    
    fp = fopen("./database/storyboard.txt", "a");
    fprintf(fp,"Username:[%s] Bet:[%d] Bid:[%d] Date:[%s]\n",topBid->elem->username,topBid->elem->bet,topBid->elem->bid,topBid->elem->date);
    fclose(fp);
    
}


void storyPlayers(char *testo, Player myPlayer, int clientid)
{
    FILE *fp;
    
    fp = fopen("./database/storyPlayers.txt", "a");
    
    if(myPlayer)
        fprintf(fp,"\nL'user %s con id %d %s %s\n",myPlayer->username,clientid,testo,dateToString());
    else
        fprintf(fp,"\nL'user con id %d %s %s\n",clientid,testo,dateToString());
    
    fclose(fp);
    
}

char *readLastResult()
{
    char buff[1024];
    char *buffer = NULL;
    
    FILE *fp;
    
    if ((fp = fopen("./database/estrazioni.txt","r"))!= NULL) // open file
    {
        fseek(fp, 0, SEEK_SET); // make sure start from 0
        
        while(!feof(fp))
        {
            memset(buff, 0x00, 1024); // clean buffer
            fscanf(fp, "%[^\n]\n", buff); // read file *prefer using fscanf
        }
        
        buffer = calloc((strlen(buff)) + 1, sizeof(char));
        strcpy(buffer,buff);
        
    }
    
    
    return buffer;
}

