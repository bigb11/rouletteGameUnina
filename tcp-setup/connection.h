#ifndef connection_h
#define connection_h


void startConnection(int sd,  struct sockaddr_in servAddr);

int acceptConnection(int sd, struct sockaddr_in clientAddr);

char *receiveMessage(int socketID);
int sendMessage(int socketID, const char* message);


#endif
