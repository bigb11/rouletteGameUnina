//
//  utils.c
//  rouletteGame
//
//  Created by Domenico Maione on 17/06/21.
//

#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../player/player.h"
#include "../string/utilsstring.h"
#include "./login-register/function-file.h"
#include "../list/arraylist.h"


//GESTIONE DELLE VINCITE

Player linkBidsOnPlayer(Player pippo,List players)
{
    while(players != NULL)
    {
        if(pippo == NULL)
            return NULL;
        
        if(strcmp(pippo->username,players->elem->username) == 0)
        {
            pippo->listBids = players->elem->listBids;
            pippo->coins = players->elem->coins;
            break;
        }
        
        
        players = players->next;
    }
    
    
    
    return pippo;
}



void scorePlayers(List bettors, int res, int idGame)
{
  
   // List curr = NULL;
    FILE *fp;
    //int i = 0;
    
    char *filename = "./database/players.txt";
    int i = 0;
    char message[100][150],buffer[150];
    Player pippo = NULL;
    
    fp = fopen(filename, "r");
    

    //metto i dati del file nella struttura data
    PlayerList arrayList = initPlayerList(100);
   
              while(fgets(buffer,150,fp))
            {
                    strcpy(message[i],buffer); //leggo la riga nel file
                    pippo = convertPlayerFromString(message[i]); //creo il player rigo per rigo preso dal file
                    pippo = linkBidsOnPlayer(pippo,bettors);
                    if(pippo != NULL)
                        addPlayerToList(arrayList,pippo); //aggiungo il player in un arrayList
                    
                    i++;
            }
    fclose(fp);
            
           // getchar();
 
            
    //printPlayerList(arrayList);
    
    fp = fopen(filename, "w+");
    
    
    for (int j = 0; j <= arrayList->indexSize; j++)
    {
        if(arrayList->listPlayers[j]->listBids != NULL)
        {
            if(arrayList->listPlayers[j]->listBids->idGame == idGame)
            {
                if(arrayList->listPlayers[j]->listBids->bet == res)
                    arrayList->listPlayers[j]->coins = (arrayList->listPlayers[j]->listBids->bid*2);
                
            }
        }
            
        
        fprintf(fp,"U:[%s] N:[%s] S:[%s] E:[%s] P:[%s] C:[%d]\n",arrayList->listPlayers[j]->username,arrayList->listPlayers[j]->name,arrayList->listPlayers[j]->surname,arrayList->listPlayers[j]->email,arrayList->listPlayers[j]->password,arrayList->listPlayers[j]->coins);
        
    }
    
    fclose(fp);
    freeArrayList(arrayList);
    
                
}
