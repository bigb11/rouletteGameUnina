


#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <signal.h>
#include "../string/utilsstring.h"
#include "../tcp-setup/connection.h"
#include "../player/player.h"
#include "../list/list.h"
#include "../utils/menu.h"
#include "../server/login-register/function-file.h"


//QUELLO GIUSTO

#define IP "127.0.0.1"
#define PORT 8080
#define SA struct sockaddr
#define BUFFER 50
//#define BUFFER_COINS 
#define STDIN 0

// - - - - - - DICHIARAZIONE VARIABILI - - - - - - //

int lenDati; int start;
int scelta=-1;
int esito;
int esito_check;
int bet,bid;
int number = 0;
int uscita = 1;
char *dati;
char *newesito;
char *esitoC;
char *esitoCheck;
char *rules;
char *lenOnline,*userOnline,*coinsOnline;
int len,i;
char *dati_player;
char *datiLogin;
char *recvStatus;
char *recvBet;
char *result;
char *recvStartGame;
char *recvLastNumber;
char *datiProfile;
char *lenOnline,*userOnline,*coinsOnline;
int taskcompleted = 0;
double numtasks = 3;


// FUNZIONE PROGRESS BAR

int show_status (double percent)
{
    int x;
    for(x = 0; x < percent; x++)
    {   
       printf("|");
              
    }
       printf("%.2f%%\r", percent);
       fflush(stdout);
       system("sleep 1");
      
    
    return(EXIT_SUCCESS);
}



void handler(int signo) 
{ 
  return; 
} 



Player myPlayer = NULL;


int main(int argc, char const *argv[])
{
    
    int sockfd;
    struct sockaddr_in servaddr;
    
    // socket create and varification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("Creazione socket fallita...\n");
        exit(0);
    }
    else
        printf("Socket creato correttamente..\n");
    
    bzero(&servaddr, sizeof(servaddr));
    
    
    
    
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);
    
    // connect the client socket to server socket
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
        printf("Connessione al server fallita...\n");
        exit(0);
    }
    else
        printf("Connesso al server..\n");
    
    
    printf("\nAPERTURA APPLICAZIONE IN CORSO...\n");
    for(; taskcompleted <= numtasks; taskcompleted++)
    show_status((taskcompleted / numtasks) * 100);
    system("clear");                     
  //----------------------------------------------

    welcome();
    
    //0 STEP
    if((sendMessage(sockfd,"P")) == 0)
    {
        close(sockfd);
    }
    
    
    do
    {
        scelta=Menu();
        
        
        //1 STEP
        if((sendMessage(sockfd,intToString(scelta))) == 0) //invia la scelta desiderata dal menu
            close(sockfd);
        
        switch (scelta)
        {
            case 0:
                printf("\nAlla prossima\n");
                exit(1);
                break;
                
            case 1:
                //REGISTRAZIONE
               
                dati = Register(dati);
    
                //2 STEP
                if((sendMessage(sockfd,dati)) == 0)
                    close(sockfd);
                
                //3 STEP
                if((esitoC = receiveMessage(sockfd)) == NULL) //riceve la scelta del menu dal client
                    close(sockfd);
                    
                
                
                
                //4 STEP
                if((dati_player = receiveMessage(sockfd)) == NULL) //ritornano dal server i dati della registrazione
                    close(sockfd);
                
                
                esito = atoi(esitoC); //trasformo in intero il risultato dell'esito della registrazione
                
                if(esito == 0)
                {
                    //crea la struttara player con i dati della registrazione
                    myPlayer = convertPlayerFromString(dati_player);
                    printf("\nRegistrazione avvenuta con successo...\n");
                    
                }
                else
                    printf("\nLa registrazione non è andata a buon fine...\n");
                
                free(esitoC);
                break;
                
            case 2:
                //LOGIN
                
                dati = Login(dati); //faccio il login
               
                //5 STEP
                if((sendMessage(sockfd,dati)) == 0)
                    close(sockfd);
                
                
                //6 STEP
                if((esitoC = receiveMessage(sockfd)) == NULL) //riceve la scelta del menu dal client
                    close(sockfd);
                
               
                //7 STEP
                if((datiLogin = receiveMessage(sockfd)) == NULL) //riceve la scelta del menu dal client
                    close(sockfd);
                
                if((esitoCheck = receiveMessage(sockfd)) == NULL) //riceve la scelta del menu dal client
                    close(sockfd);
                
                //puts(datiLogin);
                //printf("\nESITO %s\n",esitoC);
                esito = atoi(esitoC);
                //printf("\nESITO_CHECK %s\n",esitoCheck);
                esito_check = atoi(esitoCheck);
               
                if(esito == 0 && esito_check == 0)
                {
                    //crea la struttura player con i dati della registrazione
                    myPlayer = convertPlayerFromString(datiLogin);
                    printf("\nLogin avvenuto con successo...\n");
                    
                }
                else
                    printf("\nIl login non è andato a buon fine...Username/password errati oppure sei già loggato\n");
                
                free(datiLogin);
                free(esitoC);
               
                //Se il player si è loggato ed esiste allora gli affido una nuova socket per il threadSync del server
                if(myPlayer != NULL && esito == 0)
                {
                    int sync;
                    struct sockaddr_in servaddr;
                    
                    // socket create and varification
                    sync = socket(AF_INET, SOCK_STREAM, 0);
                    if (sync == -1) {
                        printf("Creazione socket fallita...\n");
                        exit(0);
                    }
                    else
                        printf("Socket creato correttamente..\n");
                    
                    bzero(&servaddr, sizeof(servaddr));
                    
                    // assign IP, PORT
                    servaddr.sin_family = AF_INET;
                    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
                    servaddr.sin_port = htons(PORT);
                    
                    // connect the client socket to server socket
                    if (connect(sync, (SA*)&servaddr, sizeof(servaddr)) != 0) {
                        printf("Connessione al server fallita...\n");
                        exit(0);
                    }
                    else
                        printf("Connesso al server per il thread Sync..\n");
                    
                    //SYNC STEP 1
                    if((sendMessage(sync,"SYNC")) == 0)
                        close(sync);
                    
                    //SYNC STEP 2
                    if((sendMessage(sync,myPlayer->username)) == 0)
                        close(sync);
                   
                    
                    
                    do
                    {
                        
                        printf("\n==================================================\n");
                        printf("\n          UTENTE: %s, COINS: %d          \n",myPlayer->username,myPlayer->coins);
                        printf("\n==================================================\n");
                        start = MenuLog(myPlayer);
                        if((sendMessage(sockfd,intToString(start))) == 0) //invio scelta del menuLog
                                    close(sockfd);
                        
                        
                        switch (start)
                        {
                            case 0:
                                printf("\nArrivederci %s...\n",myPlayer->username);
                                
                                break;
                            case 1:
                                //EFFETTUA LA GIOCATA
                                //8 STEP

                                //EFFETTUA LA GIOCATA
                                //8 STEP
                                if((sendMessage(sync,"ciao")) == 0)
                                    close(sync);
                                
                                
                                if((recvStatus = receiveMessage(sync)) == NULL)
                                    close(sync);
                                
                                puts("ATTENDI...");
                                
                                if(strcmp(recvStatus,"APERTO") == 0){
                                    if((recvBet = receiveMessage(sockfd)) == NULL)  //riceve la stringa INVIA BETS
                                        close(sockfd);
                                    
                                    printf("\n%s\n",recvBet);
                                    
                                   // puts("Inserisci il numero su cui vuoi puntare: ");
                                    
                                    struct sigaction sa;
                                    
                                    sa.sa_handler = handler;
                                    sigemptyset(&sa.sa_mask);
                                    sa.sa_flags = 0;
                                    sigaction(SIGALRM, &sa, NULL);
                                    printf("Inserire numero e importo (separati da spazio): ");
                                    alarm(15);
                                    
                                    if ((scanf("%d", &bet)== 1) && (scanf("%d",&bid)==1))
                                    {

                                         if(bet<0||bet>36||bid<0||bid>100){
                                            printf("\n**************************************************\n");
                                            printf("\n              LA TUA SCOMMESSA           ");
                                            printf("\n             ____________________          \n");
                                            printf("\n              NUMERO PUNTATO: %d                    \n",bet);
                                            printf("\n              IMPORTO SCOMMESSA: %d                 \n",bid);
                                            printf("\n              ESITO: rifiutata                    \n");
                                            printf("\n              MOTIVAZIONE: numero e/o importo errati       \n");
                                            printf("\n**************************************************\n");
                                            bet=0;
                                            bid=0;
                                        }
                                        else{
                                            if(myPlayer->coins==0||myPlayer->coins<bid){
                                                printf("\n**************************************************\n");
                                                printf("\n              LA TUA SCOMMESSA           ");
                                                printf("\n             ____________________          \n");
                                                printf("\n              NUMERO PUNTATO: %d                    \n",bet);
                                                printf("\n              IMPORTO SCOMMESSA: %d                 \n",bid);
                                                printf("\n              ESITO: rifiutata                    \n");
                                                printf("\n              MOTIVAZIONE: coins insufficienti       \n");
                                                printf("\n**************************************************\n");
                                                bet=0;
                                                bid=0;
                                            }
                                            else{
                                                printf("\n**************************************************\n");
                                                printf("\n              LA TUA SCOMMESSA           ");
                                                printf("\n             ____________________          \n");
                                                printf("\n              NUMERO PUNTATO: %d                    \n",bet);
                                                printf("\n              IMPORTO SCOMMESSA: %d                 \n",bid);
                                                printf("\n              ESITO: accettata                    \n");
                                                printf("\n**************************************************\n");
                                
                                                }

                                        alarm(0); 
                                        }
                                    }
                                    else{
                                        printf("\n**************************************************\n");
                                        printf("\n              LA TUA SCOMMESSA           ");
                                        printf("\n             ____________________          \n");
                                        printf("\n              NUMERO PUNTATO: %d                    \n",bet);
                                        printf("\n              IMPORTO SCOMMESSA: %d                 \n",bid);
                                        printf("\n              ESITO: rifiutata                    \n");
                                        printf("\n              MOTIVAZIONE: ritardo puntata            \n");
                                        printf("\n**************************************************\n");
                                        bet=0;
                                        bid=0;
                                    }
                                        
                                     if((sendMessage(sockfd,intToString(bet))) == 0)
                                        close(sockfd);
                                    
                                    
                                    if((sendMessage(sockfd,intToString(bid))) == 0)
                                        close(sockfd);
                                    
                                    
                                    
                                    printf("\nLa Roulette sta girando...\n");
                                    
                                    if((result = receiveMessage(sockfd)) == NULL)
                                        close(sockfd);
                                    
                                    
                                    printf("\n**************************************************\n");
                                    printf("\n            NUMERO VINCENTE ESTRATTO: %s          \n",result);
                                    if(strcmp(result,intToString(bet))!=0){
                                        printf("\n                HAI PERSO %d COINS                \n",bid);
                                        myPlayer->coins=myPlayer->coins-bid;
                                    }
                                    else{
                                        printf("\n                HAI VINTO %d COINS                \n",bid*2);
                                        myPlayer->coins=myPlayer->coins+bid;
                                    }
                                    
                                    printf("\n**************************************************\n");
                                    
                                }
                                else{
                                    if((result = receiveMessage(sockfd)) == NULL)
                                        close(sockfd);
                                    printf("\n**************************************************\n");
                                    printf("\n            %s          \n",result);
                                    printf("\n**************************************************\n");
                                    
                                }
                                 break;



                                
                            case 2:
                                //VISUALIZZA ESTRAZIONI PRECEDENTI
                               
                                if((recvLastNumber = receiveMessage(sockfd)) == NULL)
                                    close(sockfd);
                                printf("\n**************************************************\n");
                                printf("\n                ULTIMA ESTRAZIONE            \n");
                                printf("\n           %s                    \n",recvLastNumber);
                                printf("\n**************************************************\n");
                                
                                break;
                                
                            case 3:
                                //VISUALIZZA UTENTI
                                if((lenOnline = receiveMessage(sockfd)) == NULL)
                                    close(sockfd);
                                
                                len=atoi(lenOnline);
                                
                                printf("\n\n============ UTENTI ONLINE ============\n");
                                
                                for(i=0;i<len;i++){
                                    
                                    if(( userOnline= receiveMessage(sockfd)) == NULL)
                                        close(sockfd);
                                    
                                    if(( coinsOnline = receiveMessage(sockfd)) == NULL)
                                        close(sockfd);
                                    
                                    printf( "\n %d) Username: %s - Coins: %s \n",i+1,userOnline,coinsOnline);
                                    
                                }
                                printf("\n=======================================");
                                
                                
                                
                                break;
                                
                            case 4:
                                //VISUALIZZA PROFILO DI GIOCO
                                
                                if((datiProfile = receiveMessage(sockfd)) == NULL)
                                    close(sockfd);
                                
                                
                                Player p;
                                
                                p = convertPlayerFromString(datiProfile);
                                
                                printf("\n**************************************************\n");
                                printf("\n              DATI DEL TUO PROFILO            ");
                                printf("\n             __________________           \n");
                                printf("\n              USERNAME: %s                    \n",p->username);
                                printf("\n              NOME: %s                    \n",p->name);
                                printf("\n              COGNOME: %s                    \n",p->surname);
                                printf("\n              EMAIL: %s                    \n",p->email);
                                printf("\n              PSW: ****                    \n");
                                printf("\n**************************************************\n");
                                
                                
                                break;
                        }
                        
                        
                        
                        
                    
                    }while(start != 0); //fine del menuLog
                    
                    
                }
                
                break;
                
            case 3:
                //REGOLAMENTO GIOCO
                if((rules = receiveMessage(sockfd)) == NULL)
                    close(sockfd);
                
                printf("============== REGOLAMENTO ==============\n\n");
                puts(rules);
                break;
                
            case 4:
                //USCITA/EXIT DEL MENU
                break;
        }
        
        
        
        
    }while(scelta != 0);
    
    
    printf("\n\n");
    

    // close the socket
    close(sockfd);
    return 0;
}

