//
//  bids.c
//  rouletteGame
//
//  Created by Domenico Maione on 26/05/21.
//

#include "bids.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<stdbool.h>
#include "../string/utilsstring.h"


Bid initBid(int amount, int bet, int socket, char *username, int idGame)
{
    Bid newB = malloc(sizeof(struct TBid));

    newB->bid = amount;
    newB->bet = bet;
    newB->socket = socket;
    newB->idGame = idGame;
    newB->username = calloc(strlen(username), sizeof(char));
    strcpy(newB->username,username);
    
    char* date = dateToString();
    int len = (int) strlen(date) + 1;
    newB->date = calloc(len, sizeof(char));
    strcpy(newB->date, date);
    
    newB->next = NULL;

    return newB;
}


void addBidList(Bid *list, Bid newBid)
{
    newBid->next = *list;
    (*list) = newBid;
}

void freeBids(Bid *p)
{
    free((*p)->date);
    free((*p)->username);
    free(*p);
   
    *p = NULL;
}
