//
//  database.h
//  server
//
//  Created by Domenico Maione on 11/05/21.
//

#ifndef database_h
#define database_h

#include <stdio.h>
#include "../../list/list.h"
void createDatabase(void);
void logPlayerTxt(List listPlayer);
void write_number(int res, int idGame);

#endif /* database_h */
