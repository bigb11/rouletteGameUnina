#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<assert.h>
#include "../string/utilsstring.h"
#include "player.h"
#include "../list/list.h"
#include "../server/login-register/function-file.h"



// Player creations
Player initPlayer(char* name, char* surname, char* username, char* email, char* password, int coins)
{
    
    Player newP = malloc(sizeof(struct TPlayer));
    newP->name = calloc(strlen(name)+1,sizeof(char));
    newP->surname = calloc(strlen(surname)+1,sizeof(char));
    newP->username = calloc(strlen(username)+1,sizeof(char));
    newP->password = calloc(strlen(password)+1,sizeof(char));
    newP->email = calloc(strlen(email)+1,sizeof(char));
    
    
    strcpy(newP->name,name);
    strcpy(newP->surname,surname);
    strcpy(newP->username,username);
    strcpy(newP->password,password);
    strcpy(newP->email,email);
    newP->coins = coins;
    newP->idSocket = -1;
    
    return newP;
}



void freePlayer(Player *p)
{
    free((*p)->name);
    free((*p)->surname);
    free((*p)->email);
    free((*p)->username);
    free((*p)->password);
    free(*p);
    *p = NULL;
}





Player convertPlayerFromString(char *player)
{

    char *name = NULL;

    char *surname = NULL;

    char *email = NULL;

    char *username = NULL;

    char *password = NULL;
    
    int coins = -1;

    

    ListString list = tokenize(player, '[', ']');

    Player newP;
    


    int index = 0;



    while(list->top != NULL){

        switch(index){
                
            case 0:
                
                coins = atoi(list->top->data);
                break;
                
            case 1:

                password = calloc(strlen(list->top->data) + 1, sizeof(char));

                strcpy(password, list->top->data);

                break;

            case 2:

                email = calloc(strlen(list->top->data) + 1, sizeof(char));

                strcpy(email, list->top->data);

                break;

            case 3:

                surname = calloc(strlen(list->top->data) + 1, sizeof(char));

                strcpy(surname, list->top->data);

                break;

            case 4:

                name = calloc(strlen(list->top->data) + 1, sizeof(char));

                strcpy(name, list->top->data);

                break;

            case 5:

                username = calloc(strlen(list->top->data) + 1, sizeof(char));

                strcpy(username, list->top->data);

                break;

        }

        list->top = list->top->next;

        index++;

    }

    

    newP = initPlayer(name, surname, username, email, password, coins);

    

    free(name);

    free(surname);

    free(username);

    free(email);

    free(password);

    

    return newP;

    

}







PlayerList initPlayerList(int num_max){
    PlayerList list = malloc(sizeof(PlayerList));
    list->indexSize = -1;
    list->maxSize = num_max;
    list->listPlayers = calloc(num_max, sizeof(struct TPlayer));

    return list;
}



void addPlayerToList(PlayerList list, Player newP){
    assert(list != NULL);
    assert((list->indexSize + 1) <= list->maxSize);

    list->listPlayers[list->indexSize + 1] = newP;
    list->indexSize++;
}


void printPlayerList(PlayerList list){
    assert(list != NULL);

    printf("\nPLAYER LIST:");

    for (int i = 0; i <= list->indexSize; i++) {
       printf("\n[%d] (Player - NAME: %s, SURNAME: %s, USERNAME: %s, EMAIL: %s, PASSWORD: %s)", i+1, list->listPlayers[i]->name, list->listPlayers[i]->surname, list->listPlayers[i]->username, list->listPlayers[i]->email, list->listPlayers[i]->password);
    }
    
    printf("\n");
}


Player checkUsername(List player, char *username)
{
    List curr = player;
    while(curr != NULL)
    {
        if(strcmp(username,curr->elem->username) == 0)
            return curr->elem;
        
        
        curr = curr->next;
    }
    
    return NULL;
    
}


void freeArrayList(PlayerList p)
{
    
    for(int i = 0; i<= p->indexSize; i++)
        freePlayer(&p->listPlayers[i]);
}


List initNode(Player elem,List top)
{
    List new = malloc(sizeof(struct TList));
    new->elem = elem;
    char* date = dateToString();
    int len = (int) strlen(date) + 1;
    new->date = calloc(len, sizeof(char));
    strcpy(new->date, date);
    return new;
}



List pushList(List top,List new)
{
    if(top == NULL)
        return initNode(new->elem,top);
    
    top->elem = new->elem;
    new->next = top;
    return new;
}




List deleteListPlayer(List top, int idSocket){
    

    List tmp = NULL;
    
    if (top == NULL)
      return top;
    else
    {
        top->next = deleteListPlayer(top->next,idSocket);
        if (top->elem->idSocket == idSocket)
        {
            tmp = top;
            top = top->next;
            free(tmp);
        }
    }
    return top;
    
}









List insertHead(List top, Player key)
{
    
    List tmp;
    tmp = malloc(sizeof(struct TList));
    tmp->elem = key;
    tmp->next = top;
    return tmp;
}





BidList initNodeBid(Bid elem,BidList top)
{
    BidList new = malloc(sizeof(struct TBidList));
    new->elem = elem;
    return new;
}



BidList pushListBid(BidList top,BidList newBid)
{
    if(top == NULL)
        return initNodeBid(newBid->elem,top);
    
    newBid->next = top;
    return newBid;
}




int lenList(List top)
{
    List current = top;
    int count = 0;
    
    while(current != NULL)
    {
        count++;
        current = current->next;
    }
    
    return count;
}



void printList(List top)
{
    List curr;
    int i = 1;
    if(top == NULL)
        printf("\nLa lista è vuota\n");
    
    curr = top;
    
    while (curr != NULL) {
        printf("\nPlayer %d - NAME: %s, SURNAME: %s, USERNAME: %s, EMAIL: %s\n",i,curr->elem->name,curr->elem->surname,curr->elem->username,curr->elem->email);
        i++;
        curr = curr->next;
    }
    
    i = 0;
    
}




int checkPlayer(List top, char *username){
    

    List curr = top;

    while (curr){
        if(strcmp(curr->elem->username,username) == 0)
            return 1;
        curr = curr->next;
    }
    return 0;
}



void printBid(BidList top)
{
    BidList curr;
    
    if(top == NULL)
        printf("\nLa lista è vuota\n");
    
    curr = top;
    
    while (curr != NULL) {
        printf("\n%s %d %s\n",top->elem->username, top->elem->bet, top->elem->date);
        curr = curr->next;
    }    
    
}


void dealloca(List top){
    if(top != NULL) {
        dealloca(top->next);
        top->next=NULL;
        free(top);
        }
}
