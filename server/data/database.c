//
//  database.c
//  server
//
//  Created by Domenico Maione on 11/05/21.
//

#include "database.h"
#include <sys/stat.h>
#include <unistd.h>
#include "../../list/list.h"

void createDatabase()
{
    FILE *fp;
    struct stat st = {0};
    
    if (stat("./database", &st) == -1) {
        mkdir("./database", 0700);
    }
    
    if( access("./database/players.txt", F_OK ) != 0 )
    {
        fp = fopen("./database/players.txt","w");
        fclose(fp);
    }
    
}


void logPlayerTxt(List listPlayer)
{
    FILE *fp;
    
    fp = fopen("./database/loggers.txt", "a");
    fprintf(fp,"U:[%s] N:[%s] S:[%s] E:[%s]\n",listPlayer->elem->username,listPlayer->elem->name,listPlayer->elem->surname,listPlayer->elem->email);
    fclose(fp);
    
}

void write_number(int res, int idGame)
{
    FILE *fp;
    
    fp = fopen("./database/estrazioni.txt", "a");
    fprintf(fp,"GAME: %d - NUMERO ESTRATTO: %d\n",idGame,res);
    fclose(fp);
    
}
